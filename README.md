# Angular 5 blog

A simple angular 5 application that demonstrates various features of Angular like "Guards", "Services", "Routing". This application 
basically allows the users to create new posts, add comments to the posts, like the posts. To handle the user session, the app relies
on HTML 5 local storage and not Cookies.

## Environment

* HTML 5
* CSS
* Node.js
* Express Framework
* Mongoose for data modelling
* Body-Parser
* Angular 5
* CORS
* BootStrap framework for responsive clients

## Running the app

* Clone the repo
* Navigate to the main repo folder
* Navigate to the server folder and open a terminal at this location.
* Type `npm install` to install dependencies.
* Once installed, type `nodemon app` to start your server.
* Now from the main repo folder navigate to the src folder.
* Open another terminal at this location and type `ng serve` and point your browser to `http://localhost:4200`.
* Just as a caution, have your mongodb running prior to running the server!




