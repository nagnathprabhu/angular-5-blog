import { Component, OnInit } from '@angular/core';
import { PostService } from '../../post/post.service';
import { FormsModule } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  posts:any=[];
  user = JSON.parse(localStorage.getItem('user'));
  comments:any=[];
  showcomments:any = [];

  postSubject= new BehaviorSubject(this.posts);
  
  constructor(private postService:PostService, private form:FormsModule) {  


   }

  ngOnInit() {
    this.getPost()
   
    for(let post in this.posts){
      this.showcomments.push(false);
    }
  }

showComments(i){
  
this.showcomments[i] = !this.showcomments[i];
console.log(this.showcomments[i])
}


showDetails(i){
  alert("Title: "+this.posts[i].title+"\nDescription: "+this.posts[i].description
+"\nLikes: "+this.posts[i].likes);

}

  getPost(){
    this.postService.getPost().subscribe((data:any)=>{
console.log('called ');
this.posts=[];
      for(let dat in data){
        this.posts.push (data[dat])
        
      }
      console.log(this.posts)
      console.log(this.postSubject)
    
   })
   
  }


  increment(id,i){
    console.log(id);
    this.postService.increment(id).subscribe((data:any)=>{
console.log(data);
  this.posts[i].likes = data[0].likes;
    })

  
  }
  

  
  putComment(i,id,comment){
    console.log(comment)
    this.postService.putComments(id,this.user.user.username,comment).subscribe((data)=>{
      // this.posts[i].comments.push(data)
      this.posts[i].comments = data[0].comments
console.log(data);
      this.comments=[];
    })
   
  }



}
