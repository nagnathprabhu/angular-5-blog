import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class PostService {
posts:any;
  constructor(private http: HttpClient, private router: Router) {
  }

  //begin
  createPost(post) {
    this.http.post('http://localhost:8000/createPost', post).subscribe((data) => {
      if (data) {
        alert('Post Successful');
        this.router.navigate(['/list']);

      }

    })
  }
//end of createpost

//begining of getPost
getPost(){
  return this.http.get('http://localhost:8000/getPosts');
}
//end of get post



//function to increment the like count
increment(id){
return this.http.put('http://localhost:8000/increment',{id:id});

}

getComments(){
  return this.http.get('http://localhost:8000/getComments')
}

putComments(id,user, comment){
  
  return this.http.put('http://localhost:8000/sendComments', {id:id, comment:comment, user:user});
  
}

}
