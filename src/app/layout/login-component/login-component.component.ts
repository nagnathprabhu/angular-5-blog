import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {
  user:any={};
  

  constructor(private _authService:AuthService) { }

  ngOnInit() {
  }
authenticate(){
  
  this._authService.login(this.user);
  
}
}
