import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-register-component',
  templateUrl: './register-component.component.html',
  styleUrls: ['./register-component.component.css']
})
export class RegisterComponentComponent implements OnInit {

  user:any={};
  constructor(private _authService:AuthService) { }

  ngOnInit() {
  }

  register(){
    this._authService.register(this.user);
    
  }
}
