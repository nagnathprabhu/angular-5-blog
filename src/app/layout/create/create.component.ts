import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { PostService } from '../../post/post.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  post:any={};//object which bundles tha data sent from the form

  constructor(private postservice:PostService) {

   }

  ngOnInit() {
  }
  createPost(post){
  this.postservice.createPost(post);

  }

}
