import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';



import { AppComponent } from './app.component';
import { NavigationComponent } from './layout/navigation/navigation.component';
import { LoginComponentComponent } from './layout/login-component/login-component.component';
import { RegisterComponentComponent } from './layout/register-component/register-component.component';
import { AuthService } from './auth/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './layout/home/home.component';
import { ListComponent } from './layout/list/list.component';
import { CreateComponent } from './layout/create/create.component';
import { PostService } from './post/post.service';
import { CommentComponent } from './comment/comment.component';
import { AuthGuardService } from './auth/auth-guard.service';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponentComponent,
    RegisterComponentComponent,
    HomeComponent,
    ListComponent,
    CreateComponent,
    CommentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    RouterModule.forRoot(
      [
        {path:'login' , component:LoginComponentComponent},
        {path: 'register', component: RegisterComponentComponent},
        {path:'home', canActivate: [AuthGuardService],component:HomeComponent},
        {path:'list',canActivate: [AuthGuardService], component:ListComponent},
        {path: 'create',canActivate: [AuthGuardService], component:CreateComponent}

    
  ]
)
  ],
  providers: [AuthService, PostService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
