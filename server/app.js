
var express = require('express'),
    app = express(),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose');

var corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
}
app.use(cors(corsOptions));

app.use(bodyParser.json());
mongoose.connect('mongodb://localhost/marlabs');//connect the db
var db = mongoose.connection;//db instance, use this to listen for events
db.on('error', function () {
    console.log('Error')
});
db.on('open', function () {
    console.log("success");

});

var UserSchema = mongoose.Schema({
    "username": String,
    "password": String,
    "email": String
});

var postSchema = mongoose.Schema({
    "title": String,
    "description": String,
    "likes": {
        type: Number,
        default: 0
    },
    "comments": [{
        "cTitle": String,
        "cBy":String
     } ]
})

var User = mongoose.model('blogUser', UserSchema);
var Post = mongoose.model('posts', postSchema);


app.post('/register', function (req, res) {
    let user = new User(req.body);
    console.log(req.body);
    user.save((err, createUserObject) => {
        if (err) {
            res.status(500).send(err);
        }
        // This createdTodoObject is the same one we saved, but after Mongo
        // added its additional properties like _id.
        res.send({ 'flg': 'success' });
    });

})


app.post('/validate', function (req, res) {
    
    User.findOne({ $and: [{ username: req.body.username }, { password: req.body.password }] }, function (err, docs) {
        if (docs) {
            res.send({ "user": docs });
        }
        else {
            res.send(err);
        }
    })
})


app.post('/createPost', function (req, res) {
    
    let post = new Post(req.body);

    console.log(req.body);
    post.save((err, createPostObject) => {
        if (err) {
            res.status(500).send(err);
        }
        // This createdTodoObject is the same one we saved, but after Mongo
        // added its additional properties like _id.
        else {
            res.send({ 'flag': 'success' });
        }
    });

})


app.get('/getPosts', function (req, res) {
    Post.find({}, function (err, docs) {
        if (err) {
            res.send(err.message)
        }
        else {
            res.send(docs);
            
        }

    })
})


app.put('/increment', function (req, res) {
    console.log(req.body.id);
    Post.findByIdAndUpdate({ _id: req.body.id }, { $inc: { likes: 1 } }, function (err, docs) {


        Post.find({ _id: req.body.id }, function (err, docs) {
            if (err) {
                res.send(err)
            }
            else {
                res.send(docs);
                
            }
    
        })
    })
  
})


app.get('/', function (req, res) {
    res.send("Server works")
})

// app.get('/getComments', function(req, res){
//     Post.find({}, function(err, docs){
//         if(err){
//             res.send(err)
//         }
//         else{
       
           
//             }
        
    
//     })
// })

app.put('/sendComments', function(req,res){
    comment= {
        "cTitle": req.body.comment,
        "cBy": req.body.user
    }
    Post.findOneAndUpdate({_id:req.body.id},{$push:{comments:comment}},{new:true}, function(err,docs){
  
        Post.find({_id:req.body.id},(err,docs)=>{
            if(err){
                console.log("called")
                   res.send(err)
                
               }
               else{
                   console.log('No error');
                   
                   res.send(docs)
               }
        })


    //   if(err){
    //       res.send(err)
    //   }  
    //   else{
    //       res.send(docs)
    //   }

    })

   
})




app.listen(8000, function () {
    console.log("Server listerning on 8000");
})