import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  

  swtichNav  : Boolean = true;
  subject= new BehaviorSubject(this.getUser());

  constructor() { }
  ngOnInit() {
   
    this.swtichNav = this.getUser();
    this.subject.next(this.getUser());
    this.subject.subscribe((data)=>{
      console.log(data);
    })
    console.log("switchnav called",this.swtichNav)
  
//this.getChanges();   

  }
   
  getChanges(){
    this.subject.subscribe((data)=>{
      this.swtichNav= !this.swtichNav
    })
    
  }
  getUser() {
    
    
    var user = localStorage.getItem('user');
    var flag;
    if (user) {
      flag = true;
    }
    else{
      flag= false;
    }
    return flag;
  }

  clear() {
    localStorage.clear();
    
  }

}
