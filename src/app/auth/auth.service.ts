import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private _router: Router) { }
isLoggedIn(){
  var user = JSON.parse(localStorage.getItem('user'));
  if(user){
    return true;
  }
  else{
    window.alert('user not authenticated to access this route')
    return false;
  }
}

  register(user) {
    this.http.post('http://localhost:8000/register', user).subscribe((data) => {
      console.log("I am called");
      if (data) {
        alert('Success');
      }
    })
  }
  login(user) {
    this.http.post('http://localhost:8000/validate', user).subscribe((data) => {
      if (data) {
        localStorage.setItem('user', JSON.stringify(data));
        
        this._router.navigate(['/home']);
        
      }
      else{
        
        alert("Wrong Credentials");
      }

    })


  }

}
